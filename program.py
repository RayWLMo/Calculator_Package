from app.calculator import Calculator

query_prompt = True
first_number = float(input("Enter your first number: "))
second_number = float(input("Enter your second number: "))
while query_prompt:
    operator = input("what operation would you like to do (add, subtract, multiply, divide)?"
                     "or type 'exit' to close the program:  ")
    if operator.lower() == "add":
        print(Calculator.add(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "subtract":
        print(Calculator.subtract(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "multiply":
        print(Calculator.multiply(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "divide":
        print(Calculator.divide(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "exit":
        print("Thank you for using this program")
        query_prompt = False
    else:
        print("The input is invalid, please try again.")
