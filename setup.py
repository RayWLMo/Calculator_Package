from setuptools import setup

# Adding Information about the program

setup(name="app")
version = "1.0"
description = "Simple calculator app"
author = "Raymond Mo at Sparta Global"
author_email = "Raymond.WL.Mo@gmail.com"
url = "https://spartaglobal.com"

