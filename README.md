# Calculator Package
## Creating necessary directories and files
- Created `app` directory and `__init__.py` and `calculator.py` inside
- Created `program.py` and `setup.py`
## Completing the setup
Filled `setup.py` will all the relevant information using the setuptools module
```py
from setuptools import setup

# Adding Information about the program
setup(name="app")
version = "1.0"
description = "Simple calculator app"
author = "Raymond Mo at Sparta Global"
author_email = "Raymond.WL.Mo@gmail.com"
url = "https://spartaglobal.com"
```
# Coding `calculator.py`
- Added the basic calculator functions to `calculator.py` as a class
```python
class Calculator:

    def add(a, b):
        return a + b

    def subtract(a, b):
        return a - b

    def multiply(a, b):
        return a * b

    def divide(a, b):
        return a / b
```
## Adding functionality to be able to run the file
In `program.py`, `Calculator` class from `calculator.py` was imported
```python
from app.calculator import Calculator
```
Added the while loop and prompts to be able to run the program
```py
query_prompt = True
first_number = float(input("Enter your first number: "))
second_number = float(input("Enter your second number: "))
while query_prompt:
    operator = input("what operation would you like to do (add, subtract, multiply, divide)?"
                     "or type 'exit' to close the program:  ")
    if operator.lower() == "add":
        print(Calculator.add(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "subtract":
        print(Calculator.subtract(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "multiply":
        print(Calculator.multiply(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "divide":
        print(Calculator.divide(first_number, second_number))
        query_prompt = False
    elif operator.lower() == "exit":
        print("Thank you for using this program")
        query_prompt = False
    else:
        print("The input is invalid, please try again.")
```
## Installing the package
In Terminal window, used `pip install .` to install the program